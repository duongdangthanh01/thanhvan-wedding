const showImage = (src) => {
    const modal = document.getElementById("myModal");
    const modalImg = document.getElementById("img01");
    modal.style.display = "block";
    modalImg.src = src;
}

const closePopup = () => {
    const modal = document.getElementById("myModal");
    modal.style.display = "none";
}

document.onkeydown = function(evt) {
    evt = evt || window.event;
    let isEscape = false;
    if ("key" in evt) {
        isEscape = (evt.key === "Escape" || evt.key === "Esc");
    } else {
        isEscape = (evt.keyCode === 27);
    }
    if (isEscape) {
        closePopup();
    }
};

window.onscroll = function() {myFunction()};
function myFunction() {
    var header = document.getElementById("myHeader");
    if (window.pageYOffset >= window.innerHeight && window.innerWidth > 768) {
        header.classList.add("sticky");
    } else {
        header.classList.remove("sticky");
    }
}
